<?php

namespace App;

use Nette;
use Nette\Application\Routers\Route;
use Nette\Application\Routers\RouteList;


class RouterFactory
{
	use Nette\StaticClass;

	/**
	 * @return Nette\Application\IRouter
	 */
	public static function createRouter()
	{
		$router = new RouteList();
		$router[] = new Route('<type>/edit[/<id>]', 'AdministrationEditor:default');
		$router[] = new Route('<type>/edit/delete[/<id>]', 'AdministrationEditor:delete');
		$router[] = new Route('popularity/<mode>', 'Popularity:default');
		$router[] = new Route('[<type>/]view', 'AdministrationViewer:default');						
		$router[] = new Route('<presenter>[/<action>][/<type>]', 'AdministrationViewer:default');
		return $router;
	}
}
