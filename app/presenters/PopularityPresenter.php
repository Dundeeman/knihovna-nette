<?php

namespace App\Presenters;

use Nette\Application\UI\Presenter;
use App\Model\PopularityManager;
use Nette\Application\UI\Form;
use Nette\Utils\ArrayHash;

class PopularityPresenter extends Presenter
{
    private $popularityManager;
    private $result = null;
    
    public function __construct(PopularityManager $popularityManager)
    {
        parent::__construct();
        $this->popularityManager = $popularityManager;
    }
    public function renderDefault($mode)
    {
        $mode = ($mode==null) ? PopularityManager::ALL : $mode;
        $this->template->data = $this->popularityManager->getData($mode);
        $this->template->modes = $this->popularityManager->getModes();
    }
}
