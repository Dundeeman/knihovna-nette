<?php

namespace App\Presenters;

use Nette\Application\UI\Presenter;
use App\Model\AdministrationEditorManager;
use Nette\Application\UI\Form;
use Nette\Utils\ArrayHash;
use PDOException;

class AdministrationEditorPresenter extends Presenter
{
    private $administrationEditorManager;
    private $id;
    private $type;
    
    public function __construct(AdministrationEditorManager $administrationEditorManager)
    {
        parent::__construct();
        $this->administrationEditorManager = $administrationEditorManager;
    }
    public function renderDefault($type, $id)
    {
        $this->id=$id;
        $this->type=$type;
        $this->template->type = $type;
        $this->template->id = $id;
    }
    public function actionDelete($type, $id)
    {
        $this->administrationEditorManager->deleteData($type, $id);
        $this->redirect('AdministrationViewer:default', $type);
    }
    protected function createComponentForm()
    {
        switch ($this->getParameter('type')) {
            case AdministrationEditorManager::BOOKS:
                return $this->prepareBookForm();
            case AdministrationEditorManager::AUTHORS:
                return $this->prepareAuthorForm();
            case AdministrationEditorManager::BORROWS:
                return $this->prepareBorrowForm();
            case AdministrationEditorManager::CUSTOMERS:
                return $this->prepareCustomerForm();
            default:
                return new Form;
        }
    }
    public function formSucceeded($form, $values)
    {
        $id = $this->getParameter('id');
        $type = $this->getParameter('type');
        try {
        $this->administrationEditorManager->editData($type, $id, $values);
        $this->redirect('AdministrationViewer:default', $type);        
        }
        catch (PDOException $e){
            $this->flashMessage('Chyba databáze');
            
        }
    }

    protected function prepareBookForm()
    {
        $res = $this->administrationEditorManager->getData(AdministrationEditorManager::BOOKS, $this->getParameter('id'));
        $vals = $res->fetch();
        $form = new Form;
        $form->addText('name', 'Název:')
        ->setRequired()
        ->setDefaultValue($vals['nazev']);
        $form->addText('authors', 'Autor:')
        ->setRequired()
        ->setDefaultValue($vals['autor']);
        $form->addText('release', 'Vydani:')
        ->setRequired()
        ->setAttribute('placeholder', 'YYYY')
        ->setDefaultValue($vals['rok_vydani']);
        $form->addSubmit('save', 'Uložit');
        $form->onSuccess[] = [$this, 'formSucceeded'];
        return $form;
    }
    protected function prepareAuthorForm()
    {
        $res = $this->administrationEditorManager->getData(AdministrationEditorManager::AUTHORS, $this->getParameter('id'));
        $vals = $res->fetch();
        $form = new Form;
        $form->addText('name', 'Jméno:')
        ->setRequired()
        ->setDefaultValue($vals['jmeno']);
        $form->addText('surname', 'Příjmení:')
        ->setRequired()
        ->setDefaultValue($vals['prijmeni']);
        $form->addText('birth', 'Narozen:')
        ->setRequired()
        ->setAttribute('placeholder', 'YYYY-MM-DD')
        ->setDefaultValue($this->formatDate($vals['narozeni']));
        $form->addSubmit('save', 'Uložit');
        $form->onSuccess[] = [$this, 'formSucceeded'];
        return $form;
    }
    protected function prepareCustomerForm()
    {
        $res = $this->administrationEditorManager->getData(AdministrationEditorManager::CUSTOMERS, $this->getParameter('id'));
        $vals = $res->fetch();
        $form = new Form;
        $form->addText('name', 'Jméno:')
        ->setRequired()
        ->setDefaultValue($vals['jmeno']);
        $form->addText('surname', 'Příjmení:')
        ->setRequired()
        ->setDefaultValue($vals['prijmeni']);
        $form->addText('birth', 'Narozen:')
        ->setRequired()
        ->setDefaultValue($this->formatDate($vals['narozen']))
        ->setAttribute('placeholder', 'YYYY-MM-DD');
        $form->addText('email', 'Email:')
        ->setRequired()
        ->setDefaultValue($vals['email']);
        $form->addSubmit('save', 'Uložit');
        $form->onSuccess[] = [$this, 'formSucceeded'];
        return $form;
    }
    protected function prepareBorrowForm()
    {
        $res = $this->administrationEditorManager->getData(AdministrationEditorManager::BORROWS, $this->getParameter('id'));
        $vals = $res->fetch();
        $form = new Form;
        $form->addText('bookid', 'ID knihy:')
        ->setRequired()
        ->setDefaultValue($vals['idknihy']);
        $form->addText('customerid', 'ID zákazníka:')
        ->setRequired()
        ->setDefaultValue($vals['idzakaznika']);
        $form->addText('borrowed', 'Půjčeno:')
        ->setRequired()
        ->setDefaultValue($this->formatDate($vals['pujceno']))
        ->setAttribute('placeholder', 'YYYY-MM-DD');
        $form->addText('presumedReturn', 'Předpokládané datum vrácení:')
        ->setRequired()
        ->setDefaultValue($this->formatDate($vals['predpokladanevraceni']))
        ->setAttribute('placeholder', 'YYYY-MM-DD');
        $form->addText('return', 'Datum vrácení:')
        ->setDefaultValue($this->formatDate($vals['skutecnevraceni']))
        ->setAttribute('placeholder', 'YYYY-MM-DD');
        $form->addSubmit('save', 'Uložit');
        $form->onSuccess[] = [$this, 'formSucceeded'];
        return $form;
    }
    function formatDate($dateString)
    {
        if (!$dateString) {
            return null;
        } else {

            return \DateTime::createFromFormat('Y-m-d H:i:s', $dateString)->format('Y-m-d');
        }
    }
}
