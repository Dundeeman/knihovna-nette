<?php

namespace App\Presenters;

use Nette\Application\UI\Presenter;
use App\Model\AdministrationViewerManager;
use Nette\Application\UI\Form;
use Nette\Utils\ArrayHash;
use PDOException;

class AdministrationViewerPresenter extends Presenter
{
    private $admninistrationViewerManager;
    private $result = null;
    
    public function __construct(AdministrationViewerManager $admninistrationViewerManager)
    {
        parent::__construct();
        $this->admninistrationViewerManager = $admninistrationViewerManager;
    }
    public function renderDefault($type)
    {
        $this->template->tabs = $this->admninistrationViewerManager->getOps();
        $this->template->data = $this->admninistrationViewerManager->getTab($type);
        $this->template->type = $type;
        if ($type!=null) {
            $this->template->setFile(__DIR__ . '/templates/AdministrationViewer/'.$type.'.latte');
        }
    }
    public function actionSetupDB()
    {
        try {
            $this->admninistrationViewerManager->setupDB();
        } catch (PDOException $e) {
            $this->flashMessage('Chyba databáze');
        }
        finally
        {
            $this->redirect('AdministrationViewer:default');
        }
    }
}
