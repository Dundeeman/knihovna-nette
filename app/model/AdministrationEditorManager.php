<?php
namespace App\Model;

use DatabaseModel;

class AdministrationEditorManager extends AdministrationManager
{
    public function getOps()
    {
        return array(
        self::BOOKS => 'Knihy',
        self::AUTHORS => 'Autoři',
        self::BORROWS => 'Výpůjčky',
        self::CUSTOMERS => 'Zákazníci'
        );
    }
    public function getData($type, $id)
    {
        switch ($type) {
            case self::BOOKS:
                return $this->getBook($id);
            case self::AUTHORS:
                return $this->getAuthor($id);
            case self::BORROWS:
                return $this->getBorrow($id);
            case self::CUSTOMERS:
                return $this->getCustomer($id);
        }
    }
    public function deleteData($type, $id)
    {
        switch ($type) {
            case self::BOOKS:
                return $this->deleteBook($id);
            case self::AUTHORS:
                return $this->deleteAuthor($id);
            case self::BORROWS:
                return $this->deleteBorrow($id);
            case self::CUSTOMERS:
                return $this->deleteCustomer($id);
        }
    }
    public function editData($type, $id, $data)
    {
        switch ($type) {
            case self::BOOKS:
                return $this->editBook($id, $data);
            case self::AUTHORS:
                return $this->editAuthor($id, $data);
            case self::BORROWS:
                return $this->editBorrows($id, $data);
            case self::CUSTOMERS:
                return $this->editCustomers($id, $data);
        }
    }
    protected function getBook($id)
    {
        return $this->database->query("SELECT GROUP_CONCAT(concat_ws(' ', autori.jmeno, autori.prijmeni) SEPARATOR ', ') as autor, knihy.id,knihy.nazev,knihy.rok_vydani from knihy left join autorstvi on knihy.id = autorstvi.kniha left join autori on autorstvi.autor = autori.id where knihy.id = ? GROUP BY knihy.id ", $id);
    }
    protected function getAuthor($id)
    {
        return $this->database->query("SELECT * FROM autori where id=?", $id);        
    }
    protected function getBorrow($id)
    {
        return $this->database->query("SELECT GROUP_CONCAT(concat_ws(' ', autori.jmeno, autori.prijmeni) SEPARATOR ', ') as autor, 
        vypujcky.kniha as idknihy, knihy.nazev as nazevknihy, knihy.rok_vydani as vydano,
        concat_ws(' ', zakaznici.jmeno, zakaznici.prijmeni) as zakaznik, zakaznici.id as idzakaznika, zakaznici.email as email,
        predpokladanevraceni, skutecnevraceni, pujceno,vypujcky.id as idvypujcky
        from vypujcky
        left join knihy on vypujcky.kniha = knihy.id 
        left join autorstvi on autorstvi.kniha = vypujcky.kniha
        left join autori on autorstvi.autor = autori.id
        left join zakaznici on vypujcky.zakaznik = zakaznici.id
        where vypujcky.id = ?
        GROUP BY knihy.id,vypujcky.id", $id);        
    }
    protected function getCustomer($id)
    {
        return $this->database->query("SELECT * FROM zakaznici where id = ?", $id);        
    }
    protected function editBook($id, $data)
    {
        if ($id==null) {
            $this->database->query('INSERT into knihy (nazev,rok_vydani) values (?,?)', $data['name'], $data['release']);
            $id = $this->database->getInsertId('knihy');
        } else {
            $this->database->query('update knihy set nazev=?, rok_vydani=? where id=?', $data['name'], $data['release'], $id);
            $this->unbindAuthors($id);            
        }
        $this->bindAuthors($id, $data['authors']);
    }
    protected function editAuthor($id, $data)
    {
        if ($id==null) {
            $this->database->query('insert into autori (jmeno,prijmeni,narozeni) values (?,?,?)', $data['name'], $data['surname'],$data['birth']);
        } else {
            $this->database->query('update autori set jmeno=?, prijmeni=?, narozeni=? where id=?', $data['name'], $data['surname'],$data['birth'], $id);
        }
    }
    protected function editBorrows($id, $data)
    {
        $date = ($data['return']==''? null : $data['return']);
        if ($id==null) {
            $this->database->query('INSERT into vypujcky (kniha,zakaznik,pujceno,predpokladanevraceni,skutecnevraceni) values (?,?,?,?,?)', $data['bookid'], $data['customerid'],$data['borrowed'],$data['presumedReturn'],$date);
        } else {
            $this->database->query('UPDATE vypujcky set kniha=?, zakaznik=?, pujceno=?, predpokladanevraceni=?, skutecnevraceni=? where id=?', $data['bookid'], $data['customerid'],$data['borrowed'],$data['presumedReturn'],$date, $id);
        }
    }
    protected function editCustomers($id, $data)
    {
        if ($id==null) {
            $this->database->query('insert into zakaznici (jmeno,prijmeni,narozen,email) values (?,?,?,?)', $data['name'], $data['surname'],$data['birth'],$data['email']);
        } else {
            $this->database->query('update zakaznici set jmeno=?, prijmeni=?, narozen=?, email=? where id=?', $data['name'], $data['surname'],$data['birth'],$data['email'], $id);
        }
    }
    public function deleteBook($id)
    {
        $this->unbindAuthors($id);        
        $this->database->query('delete from knihy where id=?', $id);        
    }
    public function deleteAuthor($id)
    {
        $this->database->query('delete from autori where id=?', $id);        
        $this->unbindBooks($id);
    }
    public function deleteBorrows($id)
    {
        $this->database->query('delete from vypujcky where id=?', $id);                
    }
    public function deleteCustomers($id)
    {
        $this->database->query('delete from zakaznici where id=?', $id);                
    }
    public function unbindAuthors($id)
    {
        $this->database->query('delete from autorstvi where kniha=?', $id);
    }
    public function unbindBooks($id)
    {
        $this->database->query('delete from autorstvi where autor=?', $id);        
    }
    public function bindAuthors($id, $authorstring)
    {
        $authorstring = str_replace(", ", ",", $authorstring);        
        $authors = explode(",", $authorstring);
        foreach ($authors as $author) {
            if (strlen($author)>2) {
                $this->database->query("INSERT into autorstvi (autor,kniha) select autori.id, knihy.id from autori,knihy where instr(concat(autori.jmeno,' ',autori.prijmeni),?)>0 and knihy.id=? LIMIT 1;", $author, $id);
            }
        }
    }
}
