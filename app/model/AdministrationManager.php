<?php
namespace App\Model;

use DatabaseModel;

abstract class AdministrationManager extends DatabaseModel
{
    const
    BOOKS ='books',
    AUTHORS = 'authors',
    BORROWS = 'borrows',
    CUSTOMERS = 'customers';
}
?>