<?php
namespace App\Model;

use DatabaseModel;

class PopularityManager extends DatabaseModel
{   const
    ALL = 1,
    ACTIVE = 2;
    public function getData($mode)
    {
        switch ($mode) {
            case self::ALL:
                return $this->getAll();
            case self::ACTIVE:
                return $this->getActive();
        }
    }
    public function getModes()
    {
        return array(
        self::ALL => 'Vše',
        self::ACTIVE => 'Vypůjčené',
        );
    }
    protected function getAll()
    {
        return $this->database->query("SELECT concat_ws(' ', autori.jmeno, autori.prijmeni) as autor, 
            count(vypujcky.id) as popularita
            from vypujcky
            left join knihy on vypujcky.kniha = knihy.id 
            left join autorstvi on autorstvi.kniha = vypujcky.kniha
            left join autori on autorstvi.autor = autori.id
            group by autori.id");
    }
    protected function getActive()
    {
        return $this->database->query("SELECT concat_ws(' ', autori.jmeno, autori.prijmeni) as autor, 
            count(vypujcky.id) as popularita
            from vypujcky
            left join knihy on vypujcky.kniha = knihy.id 
            left join autorstvi on autorstvi.kniha = vypujcky.kniha
            left join autori on autorstvi.autor = autori.id
            where vypujcky.skutecnevraceni is null 
            group by autori.id");
    }
}
