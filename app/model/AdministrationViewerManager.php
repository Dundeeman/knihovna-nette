<?php
namespace App\Model;

use DatabaseModel;
use Nette\Utils\Finder;

class AdministrationViewerManager extends AdministrationManager
{
    const
    BOOKQUERY="SELECT GROUP_CONCAT(concat_ws(' ', autori.jmeno, autori.prijmeni) SEPARATOR ', ') as autor, knihy.id,knihy.nazev,knihy.rok_vydani from knihy left join autorstvi on knihy.id = autorstvi.kniha left join autori on autorstvi.autor = autori.id GROUP BY knihy.id;",
    AUTHORSQUERY="SELECT * FROM `autori`",
    BORROWSQUERY="SELECT GROUP_CONCAT(concat_ws(' ', autori.jmeno, autori.prijmeni) SEPARATOR ', ') as autor, 
    vypujcky.kniha as idknihy, knihy.nazev as nazevknihy, knihy.rok_vydani as vydano,
    concat_ws(' ', zakaznici.jmeno, zakaznici.prijmeni) as zakaznik, zakaznici.id as idzakaznika, zakaznici.email as email,
    predpokladanevraceni, skutecnevraceni, pujceno,vypujcky.id as idvypujcky
    from vypujcky
    left join knihy on vypujcky.kniha = knihy.id 
    left join autorstvi on autorstvi.kniha = vypujcky.kniha
    left join autori on autorstvi.autor = autori.id
    left join zakaznici on vypujcky.zakaznik = zakaznici.id
    GROUP BY knihy.id,vypujcky.id;",
    CUSTOMERSQUERY="SELECT * FROM `zakaznici`";
    public function getOps()
    {
        return array(
        self::BOOKS => 'Knihy',
        self::AUTHORS => 'Autoři',
        self::BORROWS => 'Výpůjčky',
        self::CUSTOMERS => 'Zákazníci'
        );
    }
    public function getTab($tab)
    {
        switch ($tab) {
            case self::BOOKS:
                return $this->database->query(self::BOOKQUERY);
            case self::AUTHORS:
                return $this->database->query(self::AUTHORSQUERY);
            case self::BORROWS:
                return $this->database->query(self::BORROWSQUERY);
            case self::CUSTOMERS:
                return $this->database->query(self::CUSTOMERSQUERY);
        }
    }

    public function setupDB()
    {

        $lines = file(__DIR__.'/knihovna.sql');
        $query='';
        foreach ($lines as $line) {
            if (substr($line, 0, 2)=='--' || substr($line, 0, 2)=='/*' || $line == '') {
                continue;
            }
              $query.= $line;
            if (substr(trim($query), -1)==';') {
                $this->database->query($query);   
                $query='';
            }
        }
    }
}
?>